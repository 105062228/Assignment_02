var menustate={
    create:function(){
        game.add.image(0,0,'background');
        //var nameLabel = game.add.text(game.width/2, 80, 'Super Coin Box', { font: '50px Arial', fill: '#ffffff' }); 
        //nameLabel.anchor.setTo(0.5, 0.5);
        var startLabel = game.add.text(game.width/2, 140, 'press enter  key to start', { font: '25px Arial', fill: '#000000' }); 
        startLabel.anchor.setTo(0.5, 0.5);
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.start, this); 
    },
    start:function(){
        game.state.start('play');
    }
};