var gameoverstate={
    create:function(){
        game.add.image(0,0,'gameover');
        var startLabel = game.add.text(game.width/2, game.height-20, 'press enter  key to replay', { font: '25px Arial', fill: '#FFFFFF' }); 
        startLabel.anchor.setTo(0.5, 0.5);
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        var escKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        escKey.onDown.add(this.menu,this);
        enterKey.onDown.add(this.start, this);
        this.checkname();
    },
    start:function(){
        game.state.start('play');
    },
    menu:function(){
        game.state.start('menu');
    },
    checkname:function(){
        var person=prompt("Please enter your name","???");
        if(person!=null){
            let timestamp=Math.floor(Date.now()/1000);
            firebase.database().ref("score").child(timestamp).set({
                name: person,
                score:game.global.score,
                time: d.toString()
            }).catch(e=>console.log(e.message));
        }
    }
};