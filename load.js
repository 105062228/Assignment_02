var loadstate={
    preload:function(){
        //game.load.image('player','assets/player.png');
        game.load.spritesheet('player','assets/player.png',32,32);
        game.load.image('wall','assets/wall.png');
        game.load.image('ceiling','assets/ceiling.png');
        game.load.image('normal','assets/normal.png');
        game.load.image('background','assets/background.png');
        game.load.image('gameover','assets/gameover.png');
        game.load.image('nails','assets/nails.png');
        game.load.spritesheet('fake','assets/fake.png',96,36);
        game.load.spritesheet('trampoline','assets/trampoline.png',96,22);
        game.load.spritesheet('conveyor_left','assets/conveyor_left.png',96,16);
        game.load.spritesheet('conveyor_right','assets/conveyor_right.png',96,16);
        game.load.image('pixel','assets/pixel.png');
    },
    create:function(){
        game.state.start('menu');
    }
};

var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas');
game.global={score:0}; 
game.state.add('load',loadstate);
game.state.add('menu',menustate);
game.state.add('play',playstate);
game.state.add('gameover',gameoverstate);
game.state.start('load');

