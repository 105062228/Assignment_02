var menustate={
    create:function(){
        game.add.image(0,0,'background');
        //var nameLabel = game.add.text(game.width/2, 80, 'Super Coin Box', { font: '50px Arial', fill: '#ffffff' }); 
        //nameLabel.anchor.setTo(0.5, 0.5);
        //var startLabel = game.add.text(game.width/2, 140, 'press enter  key to start', { font: '25px Arial', fill: '#000000' }); 
        //startLabel.anchor.setTo(0.5, 0.5);
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.start, this); 
        this.button=game.add.button(120,150,'play');
        this.button2=game.add.button(120,295,'credit');
        this.button3=game.add.button(115,225,'2player');
        //this.button.input.useHandCursor=true;
        this.button.events.onInputDown.add(this.down,this);
        this.button2.events.onInputDown.add(this.down2,this);
        this.button3.events.onInputDown.add(this.down3,this);
    },
    start:function(){
        game.state.start('play');
    },
    down:function(){
        game.state.start('play');
    },
    down2:function(){
        game.state.start('score');
    },
    down3:function(){
        game.state.start('2p');
    }
};