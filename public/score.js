var scorestate={
    create:function(){
        game.add.image(0,0,'beach');
        var name=[];
        var floor=[];
        var hard=firebase.database().ref('score').orderByChild('score');
        hard.once('value').then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                name.push(childSnapshot.val().name);
                floor.push(childSnapshot.val().score);
            });
        });
        setTimeout(function(){
            var a=name.pop();
            var b=floor.pop();
            var scoreboard=game.add.text(70, 30,'Scoreboard' , { font: '50px Arial', fill: '#0000FF' });
            var nameLabel=game.add.text(10, 100,'Top: '+a+' '+b+' Floors' , { font: '40px Arial', fill: '#FFFF00' });
            a=name.pop();
            b=floor.pop();
            var nameLabel2=game.add.text(10,150,'Number2: '+a+' '+b+' Floors' , { font: '30px Arial', fill: '#61210B' });
            a=name.pop();
            b=floor.pop();
            var nameLabel3=game.add.text(10,200,'Number3: '+a+' '+b+' Floors' , { font: '30px Arial', fill: '#61210B' });
            a=name.pop();
            b=floor.pop();
            var nameLabel4=game.add.text(10,250,'Number4: '+a+' '+b+' Floors' , { font: '30px Arial', fill: '#61210B' });
            a=name.pop();
            b=floor.pop();
            var nameLabel5=game.add.text(10,300,'Number5: '+a+' '+b+' Floors' , { font: '30px Arial', fill: '#61210B' });
            this.back=game.add.text(320,350,'返回' , { font: '30px Arial', fill: '#ffffff' });
            this.back.inputEnabled=true;
            this.back.input.useHandCursor=true;
            this.back.events.onInputDown.add(function(){
                game.state.start('menu');
            },this);
            this.back.events.onInputOver.add(function (target) {
                target.fill='#FF0000';
                target.font='40px';
            });
            this.back.events.onInputOut.add(function(target){
                target.fill='#ffffff';
                target.font='30px';
            });         
        },800);
    }
};