var twopstate={
    create:function(){
        game.add.image(0,0,'beach');
        game.global.score=0;
        //alert(game.global.score);
        this.platforms=[];
        //this.platforms2=[];
        this.lasttime=0;
        this.lastfloortime=0;
        this.lasttime2=0;
        this.distance=0;
        this.lifevalue;
        this.lifevalue2;
        this.floor;
        this.first=0;
        this.tanhuansound=game.add.audio('tanhuan');
        this.OAOsound=game.add.audio('OAO');
        this.hitsound=game.add.audio('hit');
        this.rolesound=game.add.audio('role');
        //this.OAOsound.volume=0;
        this.OAOsound.loop=false;
        game.stage.backgroundColor='#000000';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        this.cursor = game.input.keyboard.createCursorKeys();
        upKey = game.input.keyboard.addKey(Phaser.Keyboard.W);
        downKey = game.input.keyboard.addKey(Phaser.Keyboard.S);
        leftKey = game.input.keyboard.addKey(Phaser.Keyboard.A);
        rightKey = game.input.keyboard.addKey(Phaser.Keyboard.D);
        this.player=game.add.sprite(200,50,'player');
        this.player.frame=8;
        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y=500;
        this.player.life=10;
        this.player.down;
        this.player.anchor.setTo(0.5,0.5);
        this.player.animations.add('walkleft', [0, 1, 2, 3], 8, true);
        this.player.animations.add('walkright', [9, 10, 11, 12], 8, true);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 12, true);
        this.player.animations.add('flyright', [27, 28, 29, 30], 12, true);
        this.player.animations.add('fly', [36, 37, 38, 39], 12, true);
        this.player.animations.add('red',[40,41,42,43],12,true);
        this.player.animations.add('walkleftred',[4, 5, 6, 7],8,true);
        this.player.animations.add('walkrightred',[13, 14, 15, 16],8,true);
        this.player.animations.add('red2',[8, 17],8,true);

        this.player2=game.add.sprite(200,50,'player2');
        this.player2.frame=8;
        game.physics.arcade.enable(this.player2);
        this.player2.body.gravity.y=500;
        this.player2.life=10;
        this.player2.down;
        this.player2.anchor.setTo(0.5,0.5);
        this.player2.animations.add('walkleft', [0, 1, 2, 3], 8, true);
        this.player2.animations.add('walkright', [9, 10, 11, 12], 8, true);
        this.player2.animations.add('flyleft', [18, 19, 20, 21], 12, true);
        this.player2.animations.add('flyright', [27, 28, 29, 30], 12, true);
        this.player2.animations.add('fly', [36, 37, 38, 39], 12, true);
        this.player2.animations.add('red',[40,41,42,43],12,true);
        this.player2.animations.add('walkleftred',[4, 5, 6, 7],8,true);
        this.player2.animations.add('walkrightred',[13, 14, 15, 16],8,true);
        this.player2.animations.add('red2',[8, 17],8,true);

        this.emitter=game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity=500;
        this.help=game.add.sprite(140, 60, 'help'); 
        game.physics.arcade.enable(this.help); 
        this.createbounders();
        this.createtextboard();
    },
    update:function(){
        game.physics.arcade.collide(this.player, this.player2);
        game.physics.arcade.collide(this.player, this.leftwall);
        game.physics.arcade.collide(this.player, this.rightwall);
        game.physics.arcade.collide(this.player, this.platforms,this.effect,null,this);
        game.physics.arcade.overlap(this.player, this.help, this.takeCoin, null, this);
        game.physics.arcade.collide(this.player2, this.leftwall);
        game.physics.arcade.collide(this.player2, this.rightwall);
        game.physics.arcade.collide(this.player2, this.platforms,this.effect2,null,this);
        game.physics.arcade.overlap(this.player2, this.help, this.takeCoin2, null, this);
        this.updateplatforms();
        this.createplatform();
        this.calfloor();
        this.newtextboard();
        this.moveplayer();
        this.moveplayer2();
        //this.createhelp();
        this.hittop();
        if(this.player.life<=0){
            this.player.y=450;
        }
        if(this.player2.life<=0){
            this.player2.y=450;
        }
        if((this.player.life<=0&&this.player2.life<=0)||(this.player.body.position.y>450&&this.player2.body.position.y>450)||(this.player.body.position.y>450&&this.player2.life<=0)||(this.player.life<=0&&this.player2.body.position.y>450)){
            this.playerdie();
        }
    },
    createbounders:function(){
        this.ceiling=game.add.image(0, 0, 'ceiling');
        this.leftwall=game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(this.leftwall);
        this.leftwall.body.immovable=true;
        this.rightwall=game.add.sprite(383,0,'wall');
        game.physics.arcade.enable(this.rightwall);
        this.rightwall.body.immovable=true;
    },
    moveplayer:function(){
        if(this.cursor.left.isDown){
            //alert(1);
            this.player.body.velocity.x=-250;
            if(this.player.body.velocity.y>0){
                this.player.animations.play('flyleft');
            }
            else{
                if(this.player.down.key=='nails'){
                    this.player.animations.play('walkleftred');
                }
                else{
                    this.player.animations.play('walkleft');
                }
            }
        }
        else if(this.cursor.right.isDown) {
            this.player.body.velocity.x=250;
            if(this.player.body.velocity.y>0){
                this.player.animations.play('flyright');
            }
            else{
                if(this.player.down.key=='nails'){
                    this.player.animations.play('walkrightred');
                }
                else{
                    this.player.animations.play('walkright');
                }
            }
        } 
        else{
            this.player.body.velocity.x=0;
            if(this.player.body.position.y<=13){
                this.player.animations.play('red');
            }
            else{
                if(this.player.body.velocity.y!=0){
                    this.player.animations.play('fly');
                }
                else{
                    if(this.player.down.key=='nails'){
                        this.player.animations.play('red2');
                    }
                    else{
                        this.player.animations.stop();
                        this.player.frame=8;
                    }
                }
            }
        }
    },
    moveplayer2:function(){
        if(leftKey.isDown){
            //alert(1);
            this.player2.body.velocity.x=-250;
            if(this.player2.body.velocity.y>0){
                this.player2.animations.play('flyleft');
            }
            else{
                if(this.player2.down.key=='nails'){
                    this.player2.animations.play('walkleftred');
                }
                else{
                    this.player2.animations.play('walkleft');
                }
            }
        }
        else if(rightKey.isDown) {
            this.player2.body.velocity.x=250;
            if(this.player2.body.velocity.y>0){
                this.player2.animations.play('flyright');
            }
            else{
                if(this.player2.down.key=='nails'){
                    this.player2.animations.play('walkrightred');
                }
                else{
                    this.player2.animations.play('walkright');
                }
            }
        } 
        else{
            this.player2.body.velocity.x=0;
            if(this.player2.body.position.y<=13){
                this.player2.animations.play('red');
            }
            else{
                if(this.player2.body.velocity.y!=0){
                    this.player2.animations.play('fly');
                }
                else{
                    if(this.player2.down.key=='nails'){
                        this.player2.animations.play('red2');
                    }
                    else{
                        this.player2.animations.stop();
                        this.player2.frame=8;
                    }
                }
            }
        }
    },
    playerdie:function(){
        game.global.score=this.distance;
        this.OAOsound.play();
        game.time.events.add(800, function() {this.OAOsound.volume=0;game.state.start('menu');}, this);
        //game.state.start('gameover');
    },
    updateplatforms:function(){
        for(var i=0;i<this.platforms.length;i++){
            var platform=this.platforms[i];
            platform.body.position.y=platform.body.position.y-2-(this.distance/50);
            //console.log(this.distance/50);
            if(platform.body.position.y<=-20){
                platform.destroy();
                this.platforms.splice(i,1);
            }
        }
    },
    createplatform:function(){
        if(game.time.now>this.lasttime+600){
            this.lasttime=game.time.now;
            var platform;
            var x=Math.random()*(400 - 96 - 40) + 20;
            var y=400;
            var rand=Math.random() * 100;
            if(rand<20){
                platform=game.add.sprite(x, y,'normal');
            }
            else if(rand<40){
                platform=game.add.sprite(x, y,'nails');
                game.physics.arcade.enable(platform);
                platform.body.setSize(96, 15, 0, 15);
            }
            else if(rand<50){
                platform=game.add.sprite(x, y,'conveyor_left');
                platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
                platform.play('scroll');
            }
            else if(rand<60){
                platform=game.add.sprite(x, y,'conveyor_right');
                platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
                platform.play('scroll');
            }
            else if(rand<80){
                platform=game.add.sprite(x, y,'trampoline');
                platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120,false);
                platform.frame=3;
            }
            else{
                platform=game.add.sprite(x,y,'fake');
                platform.animations.add('turn',[0,1,2,3,4,5,0],14,false);
            }
            game.physics.arcade.enable(platform);
            platform.body.immovable=true;
            this.platforms.push(platform);
            platform.body.checkCollision.down=false;
            platform.body.checkCollision.left=false;
            platform.body.checkCollision.right=false;
            //this.platforms.push(platform);
        }
    },
    calfloor:function(){
        if(game.time.now>this.lastfloortime+1000){
            this.lastfloortime=game.time.now;
            this.distance+=1;
        }
    },
    createtextboard:function(){
        this.lifevalue=game.add.text(15,10,'Life:10 HP',{fill: '#ff0000', fontSize: '20px'});
        this.lifevalue2=game.add.text(15,30,'Life:10 HP',{fill: '#ff0000', fontSize: '20px'});
        this.floor=game.add.text(275,10,'Height:0 F',{fill: '#ff0000', fontSize: '20px'});
    },
    newtextboard:function(){
        this.lifevalue.setText('Life:'+this.player.life+' HP(player yellow)');
        this.lifevalue2.setText('Life:'+this.player2.life+' HP(player blue)');
        this.floor.setText('Height:'+this.distance+' F');
    },
    hittop:function(){
        if(this.player.body.position.y<=13){
            this.hitsound.play();
            this.emitter.x=this.player.x; 
            this.emitter.y=this.player.y; 
            this.emitter.start(true, 800, null, 15);
            game.camera.shake(0.02,300);
            if(this.player.down.key!='trampoline')
                this.player.body.position.y+=20;
            else{
                this.player.body.position.y+=90;
            }
            this.player.life-=3;
            this.player.body.velocity.y=0;
        }
        if(this.player2.body.position.y<=13){
            this.hitsound.play();
            this.emitter.x=this.player2.x; 
            this.emitter.y=this.player2.y; 
            this.emitter.start(true, 800, null, 15);
            game.camera.shake(0.02,300);
            if(this.player2.down.key!='trampoline')
                this.player2.body.position.y+=20;
            else{
                this.player2.body.position.y+=90;
            }
            this.player2.life-=3;
            this.player2.body.velocity.y=0;
        }
    },
    effect:function(player,platforms){
        /*if(this.first==0)
            this.player.down=platforms;*/
        if(platforms.key=='normal'){
            if(player.down!=platforms){
                if(player.life<10) {
                    player.life+=1;
                }
                player.down=platforms;
            }
        }
        if(platforms.key=='nails'){
            if(player.down!=platforms||this.first==0){
                player.life-=3;
                player.down=platforms;
                this.hitsound.play();
                this.emitter.x=player.x; 
                this.emitter.y=player.y; 
                this.emitter.start(true, 800, null, 15);
                game.camera.shake(0.02, 300);
            }

        }
        if(platforms.key=='trampoline'){
            this.tanhuansound.play();
            if(player.life<10)
                player.life+=1;
            player.body.velocity.y=(-280)+(this.distance/5);
            player.down=platforms;
        }
        if(platforms.key=='fake'){
            this.rolesound.play();
            if(player.down!=platforms||this.first==0){
                if(player.life<10)
                    player.life+=1;
                platforms.animations.play('turn');
                setTimeout(function(){
                    platforms.body.checkCollision.up=false;
                }, 100);
                player.down=platforms;
            }
        }
        if(platforms.key=='conveyor_left'){
            player.body.position.x-=2;
            if(player.down!=platforms){
                if(player.life<10)
                    player.life+=1;
                player.down=platforms;
            }
        }
        if(platforms.key=='conveyor_right'){
            player.body.position.x+=2;
            if(player.down!=platforms){
                if(player.life<10)
                    player.life+=1;
                player.down=platforms;
            }
        }
        this.first++;
    },
    takeCoin:function(){
        var coinPosition = [{x: 140, y: 60}, {x: 300, y: 60}, {x: 60, y: 140}, {x: 200, y: 140}, {x: 130, y: 300}, {x: 320, y: 300}]; 
        for (var i = 0; i < coinPosition.length; i++) { 
            if (coinPosition[i].x == this.help.x) { 
                coinPosition.splice(i, 1); 
            } 
        } 
        var newPosition=game.rnd.pick(coinPosition);          
        this.help.reset(newPosition.x, newPosition.y);
        this.player.life+=1;
    },
    takeCoin2:function(){
        var coinPosition = [{x: 140, y: 60}, {x: 300, y: 60}, {x: 60, y: 140}, {x: 200, y: 140}, {x: 130, y: 300}, {x: 320, y: 300}]; 
        for (var i = 0; i < coinPosition.length; i++) { 
            if (coinPosition[i].x == this.help.x) { 
                coinPosition.splice(i, 1); 
            } 
        } 
        var newPosition=game.rnd.pick(coinPosition);          
        this.help.reset(newPosition.x, newPosition.y);
        this.player2.life+=1;
    },
    effect2:function(player,platforms){
        if(platforms.key=='normal'){
            if(player.down!=platforms){
                if(player.life<10) {
                    player.life+=1;
                }
                player.down=platforms;
            }
        }
        if(platforms.key=='nails'){
            if(player.down!=platforms){
                player.life-=3;
                player.down=platforms;
                this.hitsound.play();
                this.emitter.x=player.x; 
                this.emitter.y=player.y; 
                this.emitter.start(true, 800, null, 15);
                game.camera.shake(0.02, 300);
            }

        }
        if(platforms.key=='trampoline'){
            this.tanhuansound.play();
            if(player.life<10)
                player.life+=1;
            player.body.velocity.y=(-280)+(this.distance/5);
            player.down=platforms;
        }
        if(platforms.key=='fake'){
            this.rolesound.play();
            if(player.down!=platforms){
                if(player.life<10)
                    player.life+=1;
                platforms.animations.play('turn');
                setTimeout(function(){
                    platforms.body.checkCollision.up=false;
                }, 100);
                player.down=platforms;
            }
        }
        if(platforms.key=='conveyor_left'){
            player.body.position.x-=2;
            if(player.down!=platforms){
                if(player.life<10)
                    player.life+=1;
                player.down=platforms;
            }
        }
        if(platforms.key=='conveyor_right'){
            player.body.position.x+=2;
            if(player.down!=platforms){
                if(player.life<10)
                    player.life+=1;
                player.down=platforms;
            }
        }
    }
};